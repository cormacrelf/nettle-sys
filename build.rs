use std::env;
use std::fs;
use std::path::{Path, PathBuf};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[cfg(all(target_env = "msvc", not(feature = "vendored")))]
fn try_vcpkg() -> Result<Vec<PathBuf>> {
    let lib = vcpkg::Config::new()
        .emit_includes(true)
        .find_package("nettle")?;

    Ok(lib.include_paths)
}

#[cfg(any(not(target_env = "msvc"), feature = "vendored"))]
fn try_vcpkg() -> Result<Vec<PathBuf>> { Err("not applicable")?; unreachable!() }

fn print_library(lib: &pkg_config::Library, mode: &str) {
	for p in &lib.include_paths {
		println!("cargo:include={}", p.display());
	}

	for p in &lib.frameworks {
		println!("cargo:rustc-link-lib=framework={}", p);
	}

	for p in &lib.framework_paths {
		println!("cargo:rustc-link-search=framework={}", p.display());
	}

    for p in &lib.libs {
        println!("cargo:rustc-link-lib={}={}", mode, p);
    }

    for p in &lib.link_paths {
        println!("cargo:rustc-link-search=native={}", p.display());
    }
}

fn try_pkg_config() -> Result<Vec<PathBuf>> {
    let mut nettle = pkg_config::probe_library("nettle hogweed")?;

    let mode = match env::var_os("NETTLE_STATIC") {
        Some(_) => "static",
        None => "dylib",
    };

    print_library(&nettle, mode);

    // GMP only got pkg-config support in release 6.2 (2020-01-18). So we'll try to use it if
    // possible, but fall back to just looking for it in the default include and lib paths.
    if let Ok(mut gmp) = pkg_config::probe_library("gmp") {
        print_library(&gmp, mode);
        nettle.include_paths.append(&mut gmp.include_paths);
    } else {
        println!("cargo:rustc-link-lib={}=gmp", mode);
    }

    Ok(nettle.include_paths)
}

const NETTLE_PREGENERATED_BINDINGS: &str = "NETTLE_PREGENERATED_BINDINGS";

fn real_main() -> Result<()> {
    println!("cargo:rerun-if-env-changed=NETTLE_STATIC");
    println!("cargo:rerun-if-env-changed={}", NETTLE_PREGENERATED_BINDINGS);

    #[cfg(feature = "vendored")]
    {
        let artifacts = nettle_src::Build::new().build();
        println!("cargo:vendored=1");
        env::set_var("PKG_CONFIG_PATH", artifacts.lib_dir().join("pkgconfig"));
    }

    let include_paths = try_vcpkg().or_else(|_| try_pkg_config())?;

    let out_path = Path::new(&env::var("OUT_DIR").unwrap()).join("bindings.rs");

    // Check if we have a bundled bindings.rs.
    if let Ok(bundled) = env::var(NETTLE_PREGENERATED_BINDINGS)
    {
        let p = Path::new(&bundled);
        if p.exists() {
            fs::copy(&p, &out_path)?;
            println!("cargo:rerun-if-changed={:?}", p);

            // We're done.
            return Ok(());
        }
    }

    let mut builder = bindgen::Builder::default()
        // Includes all nettle headers except mini-gmp.h
        .header("bindgen-wrapper.h")
        // Workaround for https://github.com/rust-lang-nursery/rust-bindgen/issues/550
        .blacklist_type("max_align_t")
        .blacklist_function("strtold")
        .blacklist_function("qecvt")
        .blacklist_function("qfcvt")
        .blacklist_function("qgcvt")
        .blacklist_function("qecvt_r")
        .blacklist_function("qfcvt_r")
        .size_t_is_usize(true);

    for p in include_paths {
        builder = builder.clang_arg(format!("-I{}", p.display()));
    }

    let bindings = builder.generate().unwrap();
    bindings.write_to_file(out_path)?;

    Ok(())
}

fn main() -> Result<()> {
    match real_main() {
        Ok(()) => Ok(()),
        Err(e) => {
            eprintln!("Building nettle-sys failed.");
            eprintln!("Because: {}", e);
            eprintln!();
            print_hints();
            Err("Building nettle-sys failed.")?;
            unreachable!()
        }
    }
}

fn print_hints() {
    eprintln!("Please make sure the necessary build dependencies are \
               installed.\n");

    if cfg!(target_os = "windows") {
        eprintln!("If you are using MSYS2, try:

    $ pacboy -S clang:x nettle:x
");
    } else if cfg!(target_os = "macos") {
        eprintln!("If you are using MacPorts, try:

    $ sudo port install nettle pkgconfig
");
    } else if cfg!(target_os = "linux") {
        eprintln!("If you are using Debian (or a derivative), try:

    $ sudo apt install clang llvm pkg-config nettle-dev
");

        eprintln!("If you are using Arch (or a derivative), try:

    $ sudo pacman -S clang pkg-config nettle --needed
");

        eprintln!("If you are using Fedora (or a derivative), try:

    $ sudo dnf install clang pkg-config nettle-devel
");
    }

    eprintln!("See https://gitlab.com/sequoia-pgp/nettle-sys#building \
               for more information.\n");
}
